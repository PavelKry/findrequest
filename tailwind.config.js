module.exports = {
  purge: ['./src/**/*.js', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        tan: {
          light: '#FFEADB',
          DEFAULT: '#D2B48C',
          dark: '#775D37',
        },
      },
      spacing: {
        18: '4.5rem',
      },
      maxHeight: {
        18: '4.5rem',
      },
      height: {
        screen: 'calc(100 * var(--vh))',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
