import React from 'react';
import { render } from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';

import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from './application/store/configure-store';
//import App, { reducers as appReducers, sagas as appSagas } from './App';
import compileRootReducer from './application/store/compile-root-reducer';
import compileRootSaga from './application/store/compile-root-saga';
import sagaMiddleware from './application/store/enhancers/middlewares/saga';
import reportWebVitals from './reportWebVitals';

const store = configureStore(compileRootReducer(appReducers));
/*let task = sagaMiddleware.run(compileRootSaga(appSagas));*/

const renderApp = () =>
  render(
    <React.StrictMode>
      <Provider store={store}>
        <Router>
          <App />
        </Router>
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./App', () => {
    store.replaceReducer(compileRootReducer(appReducers));
    task.cancel();
    task = sagaMiddleware.run(compileRootSaga(appSagas));
    renderApp();
  });
}

renderApp();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
