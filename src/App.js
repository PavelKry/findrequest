import React from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';

import FindRequest from "./entities/components/FindRequest";
/*import WelcomeScenario, {
  sagas as welcomeScenarioSagas,
  reducers as welcomeScenarioReducers,
} from './entities/components/WelcomeScenario';
import Challenges from './entities/components/Challenges';
import SignIn from './entities/components/SignInPage';
import InvitePage from './entities/components/InvitePage';*/

const App = () => {
  console.log('🌠️ Render App');
  const { pathname } = useLocation(); // https://jasonwatmore.com/post/2020/03/23/react-router-remove-trailing-slash-from-urls

  return (
    <Switch>
      <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
      {/*<Route path="/signin">
        <SignIn />
      </Route>
      <Route path="/challenges">
        <Challenges />
      </Route>
      <Route path="/invite">
        <InvitePage />
      </Route>*/}
      <Route exact path="/">
        <FindRequest />
      </Route>

      {/* when none of the above match, <NoMatch> will be rendered */}
      <Route>404 Not Found</Route>
    </Switch>
  );
};
/*export const sagas = new Set([...welcomeScenarioSagas]);

export const reducers = new Set([...welcomeScenarioReducers]);*/

export default App;
