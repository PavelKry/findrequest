export default {
  userServiceHost: 'http://127.0.0.1:3001',
  // userServiceHost: 'https://192.168.31.76:3001',
  staticHost: 'http://localhost:3002',
  domain: 'dambledine.com',
};
