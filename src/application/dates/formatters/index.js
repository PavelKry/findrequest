import isToday from 'src/application/dates/checks/isToday';

export const formatToHuman = (date) => {
  return new Intl.DateTimeFormat('ru', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
  }).format(date);
};

export const formatTimeToHuman = (date) => {
  let options = {
    hour: 'numeric',
    minute: 'numeric',
  };
  if (!isToday(date)) {
    options = {
      ...options,
      ...{
        year: 'numeric',
        month: 'short',
        day: '2-digit',
      },
    };
  }

  return new Intl.DateTimeFormat('ru', options).format(date);
};
