import { fork, all } from 'redux-saga/effects';

export default (sagas) => {
  const registeredSagas = {};

  for (const saga of sagas) {
    // 1. Check that every saga has a name

    if (!saga.name) {
      throw new Error(
        `Every saga has to have name property. For debug purposes: ${saga.toString()}`
      );
    }

    // 2. Check that we have only unique sagas (one could be from features, another the same one from shared)
    if (registeredSagas[saga.name]) {
      throw new Error(
        `That saga ${saga.name} was already registered. Only 1 instance for particular name is allowed`
      );
    }
    registeredSagas[saga.name] = fork(saga);
  }

  return function* rootSaga() {
    yield all(registeredSagas);
  };
};
