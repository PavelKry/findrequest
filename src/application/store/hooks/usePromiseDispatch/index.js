import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

export default function usePromiseDispatch() {
  const dispatch = useDispatch();

  return useCallback(
    (action) =>
      new Promise((resolve, reject) => {
        action.resolve = resolve;
        action.reject = reject;
        dispatch(action);
      }),
    [dispatch]
  );
}
