export default (cases) => (defaultState) => (state = defaultState, { type, payload }) => {
  if (cases[type]) {
    return cases[type](state, payload);
  }
  return state;
};
