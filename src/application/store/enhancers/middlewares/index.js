import { applyMiddleware } from 'redux';
import loggerMiddleware from './logger';
import sagaMiddleware from './saga';

const middlewareEnhancer = applyMiddleware(loggerMiddleware, sagaMiddleware);

export default middlewareEnhancer;
