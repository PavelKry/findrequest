import middlewareEnhancer from './middlewares';
import monitorReducerEnhancer from './monitor-reducer';

export default [middlewareEnhancer, monitorReducerEnhancer];
