import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import enhancers from './enhancers';

export default (rootReducer, preloadedState) => {
  return createStore(
    rootReducer,
    preloadedState,
    // compose(...enhancers),
    composeWithDevTools(...enhancers)
  );
};
