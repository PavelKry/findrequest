import { combineReducers } from 'redux';

function pasteReducer(sourceObj, reducer) {
  const pathItems = reducer.path.split('.'); // ['entities', 'positions', 'byId']
  let result = sourceObj;
  for (let i = 0; i < pathItems.length; i++) {
    if (i + 1 < pathItems.length) {
      if (!result[pathItems[i]]) {
        result[pathItems[i]] = {};
      } else if (typeof result[pathItems[i]] === 'function') {
        throw new Error(
          `Reducer override. High probability of bugs. Check why the following path overrides all the down reducers ${reducer.path
            .splice(0, i)
            .join('.')}`
        );
      }
      result = result[pathItems[i]];
    } else {
      // Handle the last path item
      result[pathItems[i]] = reducer;
    }
  }
}

function combineAllRecursively(reducersByPath) {
  Object.keys(reducersByPath).forEach((key) => {
    // { positions: ...,  }
    if (typeof reducersByPath[key] === 'object') {
      reducersByPath[key] = combineAllRecursively(reducersByPath[key]);
    }
  });

  return combineReducers(reducersByPath);
}

export default (reducers) => {
  // Set(function(state, action) + path, function(state, action) + path)
  const usedPaths = {};
  const reducersByPath = {};
  for (const reducer of reducers) {
    // 1. Check that every reducer has path
    if (!reducer.path) {
      throw new Error(
        `Every reducer has to have path property. For debug purposes: ${reducer.toString()}`
      );
    }

    // 2. Check that we have only unique reducers (one could be from features, another the same one from shared)
    if (usedPaths[reducer.path]) {
      throw new Error(
        `That reducer for path ${reducer.path} was already registered. Only 1 instance for particular path is allowed`
      );
    }
    usedPaths[reducer.path] = true;

    pasteReducer(reducersByPath, reducer);
  }

  return combineAllRecursively(reducersByPath);
};

/*
{
  'entities': {
    'positions': {
      'byId': function(state, action)
      'currentPositions': function(state, action)
    },
    'users: {
      'byId': function(state, action),
      'active': {
        'byId': function(state, action),
        'alltogether': {
          'hello: function(state, action),
          'bye': function(state, action)
        }
      }
    },

  }
  'application': {
    _reducers: [],

  }
  'ui': {
    _reducers: [],
  }
}
*/
